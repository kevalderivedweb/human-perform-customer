package com.humanperform.customer.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.humanperform.customer.Adapter.AdapterCustomersDetails;
import com.humanperform.customer.R;
import com.humanperform.customer.TrainingDetailsActivity;

public class ActiveProduct extends Fragment {


    private RecyclerView recBookedActive;
    private AdapterCustomersDetails adapterCustomersDetails;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.active_product, container, false);


        recBookedActive = view.findViewById(R.id.recBookedActive);
        recBookedActive.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterCustomersDetails = new AdapterCustomersDetails(getActivity(), new AdapterCustomersDetails.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                startActivity(new Intent(getActivity(), TrainingDetailsActivity.class));
            }
        });
        recBookedActive.setAdapter(adapterCustomersDetails);


        return view;
    }


}