package com.humanperform.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.humanperform.customer.Adapter.AdapterPreferredCoachInner;

public class Preferred_coach_inner extends AppCompatActivity {

    private RecyclerView recPrefferedCoachInner;
    private AdapterPreferredCoachInner adapterPreferredCoachInner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferred_coach_inner);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Preferred_coach_inner.this,R.color.red));

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recPrefferedCoachInner = findViewById(R.id.recPrefferedCoachInner);
        recPrefferedCoachInner.setLayoutManager(new GridLayoutManager(this, 2));
        adapterPreferredCoachInner = new AdapterPreferredCoachInner(this, new AdapterPreferredCoachInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recPrefferedCoachInner.setAdapter(adapterPreferredCoachInner);


    }
}