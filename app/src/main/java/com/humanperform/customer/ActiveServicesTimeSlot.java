package com.humanperform.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class ActiveServicesTimeSlot extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_services_time);


        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(ActiveServicesTimeSlot.this, R.color.red));


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.start_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog("Start Date");
            }
        });
        findViewById(R.id.end_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog("End Date");
            }
        });


    }

    private void openDialog(String text) {


        LinearLayout weeklyBook, yearlyBook;
        TextView weeklyText, yearlyText;


        Dialog dialogForCity;
        dialogForCity = new Dialog(ActiveServicesTimeSlot.this);
        dialogForCity.setContentView(R.layout.custom_dialog_start_end_date);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);



        /* start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        final Calendar defaultSelectedDate = Calendar.getInstance();



        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(dialogForCity, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                .formatTopText("EE")
                .formatMiddleText("dd")
                //   .formatBottomText("MMM")
                .selectedDateBackground(getResources().getDrawable(R.drawable.black_round_corner))
                .showTopText(true)
                .showBottomText(false)
                .textColor(Color.parseColor("#102759"), Color.WHITE)
                .colorTextMiddle(Color.parseColor("#102759"), Color.WHITE)
                .selectorColor(Color.parseColor("#0000ffff"))
                .end()

                .defaultSelectedDate(defaultSelectedDate)
                /* .addEvents(new CalendarEventsPredicate() {

                     Random rnd = new Random();
                     @Override
                     public List<CalendarEvent> events(Calendar date) {
                         List<CalendarEvent> events = new ArrayList<>();
                         int count = rnd.nextInt(6);

                         for (int i = 0; i <= count; i++){
                             events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                         }

                         return events;
                     }
                 })*/
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {

                String selectedDateStr = DateFormat.format("MMM yyyy", date).toString();
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);


            }

        });




        TextView textzz = dialogForCity.findViewById(R.id.text);
        textzz.setText(text);
        weeklyBook = dialogForCity.findViewById(R.id.weeklyBook);
        yearlyBook = dialogForCity.findViewById(R.id.yearlyBook);
        weeklyText = dialogForCity.findViewById(R.id.weeklyText);
        yearlyText = dialogForCity.findViewById(R.id.yearlyText);
        HorizontalCalendarView calendarView = dialogForCity.findViewById(R.id.calendarView);
        CalendarView calendarView_year = dialogForCity.findViewById(R.id.calendarView_year);

        calendarView.setVisibility(View.VISIBLE);
        calendarView_year.setVisibility(View.GONE);


        weeklyBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarView.setVisibility(View.VISIBLE);
                calendarView_year.setVisibility(View.GONE);
                weeklyBook.setBackground(getResources().getDrawable(R.drawable.corner_round_5));
                yearlyBook.setBackground(getResources().getDrawable(R.drawable.corner_round_5_white));
                weeklyText.setTextColor(ContextCompat.getColor(ActiveServicesTimeSlot.this, R.color.white));
                yearlyText.setTextColor(ContextCompat.getColor(ActiveServicesTimeSlot.this, R.color.black));

            }
        });

        yearlyBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarView.setVisibility(View.GONE);
                calendarView_year.setVisibility(View.VISIBLE);
                weeklyBook.setBackground(getResources().getDrawable(R.drawable.corner_white_5));
                yearlyBook.setBackground(getResources().getDrawable(R.drawable.corner_red_2));
                weeklyText.setTextColor(ContextCompat.getColor(ActiveServicesTimeSlot.this, R.color.black));
                yearlyText.setTextColor(ContextCompat.getColor(ActiveServicesTimeSlot.this, R.color.white));

            }
        });


        dialogForCity.findViewById(R.id.dialogClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogForCity.dismiss();

            }
        });


        dialogForCity.show();
    }


}