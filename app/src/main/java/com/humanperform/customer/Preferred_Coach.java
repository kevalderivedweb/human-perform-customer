package com.humanperform.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.humanperform.customer.Adapter.AdapterPreferredCoach;

public class Preferred_Coach extends AppCompatActivity {

    private RecyclerView recPrefferedCoach;
    private AdapterPreferredCoach adapterPreferredCoach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferred_coach);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Preferred_Coach.this,R.color.red));

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        recPrefferedCoach = findViewById(R.id.recPrefferedCoach);
        recPrefferedCoach.setLayoutManager(new LinearLayoutManager(this));
        adapterPreferredCoach = new AdapterPreferredCoach(this, new AdapterPreferredCoach.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(Preferred_Coach.this, Preferred_coach_inner.class));
            }
        });
        recPrefferedCoach.setAdapter(adapterPreferredCoach);



    }
}